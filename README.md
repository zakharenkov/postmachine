# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This is a C++/QT implementation (still being developed though) of Post Machine.
Post Machine is a model of commands executor with primitive set of commands and infinite memory.
Read more at https://en.wikipedia.org/wiki/Post%E2%80%93Turing_machine

### How do I get set up? ###

The project is cross-platform. You must have QT5 libs installed. Use "qmake" command to build.
