#include "postmachineexecutor.h"

#include <QDataStream>
#include <iostream>
using std::cout;
using std::endl;

QString PostMachineCommand::refsToString() const {
    QString answer;
    if (this->ref1 >= 0)
        answer = QString::number(this->ref1 + 1);
    if (this->ref2 >= 0)
        answer += QString(",") + QString::number(this->ref1 + 1);
    return answer;
}

PostMachineExecutor::PostMachineExecutor()
{
    currentCommandIndex = -1;
    caretPosition = -1;
    running = false;
}

void PostMachineExecutor::clearCommands()
{
    this->commands.clear();
}

void PostMachineExecutor::addCommand(PostMachineCommand::Type type, int ref1, int ref2)
{
    this->commands.push_back(PostMachineCommand(type, ref1, ref2));
}

void PostMachineExecutor::setMark(int index)
{
    Q_ASSERT(index >= 0 && index < strip.size());

    if (!strip[index])
    {
        strip[index] = true;
        //Q_EMIT markSet(index);
    }
}

void PostMachineExecutor::prepareRun(const QVector<PostMachineCommand>& commands,
                                     const QVector<bool>& checkedStates,
                                     int stripCaretPosition)
{
    Q_ASSERT(commands.size() > 0);
    this->commands = commands;
    cout << "prepareRun with stripCaretPosition = " << stripCaretPosition << endl;
    Q_ASSERT(stripCaretPosition >= 0 && stripCaretPosition < checkedStates.size());
    this->strip = checkedStates;
    this->caretPosition = stripCaretPosition;
    this->currentCommandIndex = 0;
    this->running = true;
}

PostMachineExecutor::CommandExecutionStatus PostMachineExecutor::executeNextCommand()
{
    Q_ASSERT(running);
    Q_ASSERT(currentCommandIndex < commands.size());

    const PostMachineCommand& command = commands[currentCommandIndex];

    switch (command.type)
    {
        case PostMachineCommand::TYPE_LEFT:
            if (caretPosition == 0)
                return ERROR_CANNOT_MOVE_LEFT;
            --caretPosition;
            Q_EMIT caretMovedLeft();
            currentCommandIndex = command.ref1;
            break;
        case PostMachineCommand::TYPE_RIGHT:
            if (caretPosition == strip.length() - 1)
                return ERROR_CANNOT_MOVE_RIGHT;
            ++caretPosition;
            Q_EMIT caretMovedRight();
            currentCommandIndex = command.ref1;
            break;
        case PostMachineCommand::TYPE_SET_MARK:
            if (strip[caretPosition])
                return ERROR_CANNOT_SET_MARK;
            strip[caretPosition] = true;
            currentCommandIndex = command.ref1;
            Q_EMIT markWasSet();
            break;
        case PostMachineCommand::TYPE_UNSET_MARK:
            if (!strip[caretPosition])
                return ERROR_CANNOT_UNSET_MARK;
            strip[caretPosition] = false;
            currentCommandIndex = command.ref1;
            Q_EMIT markWasUnset();
            break;
        case PostMachineCommand::TYPE_CHOICE:
            if (!strip[caretPosition])
                currentCommandIndex = command.ref1;
            else
                currentCommandIndex = command.ref2;
            break;
        case PostMachineCommand::TYPE_STOP:
            running = false;
            Q_EMIT stop();
            break;
    }

    if (running)
    {
        if (currentCommandIndex < 0 || currentCommandIndex >= strip.size())
            return ERROR_BAD_REFERENCE;

        Q_EMIT goToNextCommand(currentCommandIndex);
    }

    return COMMAND_OK;
}

QDataStream& operator<<(QDataStream& stream, const PostMachineCommand& command)
{
    stream << (qint32)command.type << (qint32)command.ref1 << (qint32)command.ref2;
    return stream;
}

QDataStream& operator>>(QDataStream& stream, PostMachineCommand& command)
{
    qint32 type, ref1, ref2;
    stream >> type >> ref1 >> ref2;
    command.type = (PostMachineCommand::Type)type;
    command.ref1 = ref1;
    command.ref2 = ref2;
    return stream;
}