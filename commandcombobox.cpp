#include "commandcombobox.h"
#include <QApplication>

#include <iostream>
using std::cout;
using std::endl;

CommandComboBox::CommandComboBox(QWidget* parent) : QComboBox(parent)
{
    this->addItem("");
    this->addItem(QIcon(":/icons/arrow-left-thick-24.png"), this->tr("left"));
    this->addItem(QIcon(":/icons/arrow-right-thick-24.png"), this->tr("right"));
    this->addItem(QIcon(":/icons/apply-24.png"), this->tr("set mark"));
    this->addItem(QIcon(":/icons/arrow-up-down-24.png"), this->tr("remove mark"));
    this->addItem(QIcon(":/icons/question-mark-24.png"), this->tr("choice"));
    this->addItem(QIcon(":/icons/exclamation-mark-24.png"), this->tr("stop"));
}

CommandComboBox::~CommandComboBox()
{
  //  cout << "~CommandComboBox" << endl;
}

void CommandComboBox::retranslateUi()
{
    this->setItemText(1, this->tr("left"));
    this->setItemText(2, this->tr("right"));
    this->setItemText(3, this->tr("set mark"));
    this->setItemText(4, this->tr("remove mark"));
    this->setItemText(5, this->tr("choice"));
    this->setItemText(6, this->tr("stop"));
}
