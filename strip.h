#ifndef STRIP_H
#define STRIP_H

#include <QWidget>
#include <QLineEdit>
#include <QIntValidator>
#include <QShowEvent>
#include <QTimer>

namespace Ui {
class Strip;
}

class Strip : public QWidget
{
    Q_OBJECT

public:
    explicit Strip(QWidget *parent = 0);
    ~Strip();
    int getLength() const;
    int getCaretIndex() const; // is non-negative index of the active checkbox
    int getCaretPosition() const; // the number as text on the caret label
    QVector<bool> getCheckedStates() const;
    void setCheckedStates(const QVector<bool>& checkedStates);
    
public Q_SLOTS:
    void moveTo(int caretPosition);
    
protected Q_SLOTS:
    void onScrollBarValueChanged(int position);
    void caretTextChanged(const QString& text);
    void moveCaretLeft();
    void moveCaretRight();
    void initialPositioning();
    void toggleCheckbox();

protected:
    QLineEdit* caret;
    QIntValidator* caretValidator;

    // overridden virtual function
    void resizeEvent(QResizeEvent* event);
    void showEvent(QShowEvent* event);

    static const int CHECKBOX_WIDTH = 20;

    
private:
    int maxPosition; // possible positions are in range [-maxPosition; maxPosition]
    Ui::Strip *ui;
    QTimer * initTimer; // to postpone srollAreaContentsWidget initial positioning
};

#endif // STRIP_H
