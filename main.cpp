#include <QApplication>
#include <QFile>
#include <QDesktopWidget>
#include <QTranslator>

#include "mainwindow.h"
#include "strip.h"

#include <iostream>
using std::cout;
using std::endl;

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QFile file(":/stylesheet.qss");
    file.open(QFile::ReadOnly);
    QString styleSheet = QLatin1String(file.readAll());
    file.close();
    a.setStyleSheet(styleSheet);

    //QTranslator myTranslator;
    //myTranslator.load("postmachine_" + QLocale::system().name());
    //a.installTranslator(&myTranslator);

    MainWindow w;
    w.move(QApplication::desktop()->screen()->rect().center() - w.rect().center());
    w.show();

    cout << QLocale::system().name().toStdString() <<endl;

    return a.exec();
}
