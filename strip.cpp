#include "strip.h"
#include "ui_strip.h"
#include "postmachineexecutor.h"
#include <QMessageBox>
#include <QLabel>
#include <QCheckBox>
#include <QResizeEvent>
#include <cassert>

#include <iostream>
using std::cout;
using std::endl;

Strip::Strip(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Strip)
{
    ui->setupUi(this);
    
    this->maxPosition = 50; // TODO: allow user to change this value. Or increase strip by necessity.

    for (int i = -maxPosition; i <= maxPosition; ++i)
    {

        QCheckBox* checkBox = new QCheckBox(ui->checkboxes);
        checkBox->setObjectName(QStringLiteral("checkbox_%1").arg(i));
        ui->checkboxes_layout->addWidget(checkBox);
    }

    for (int i = -maxPosition; i <= maxPosition; ++i)
    {
        QLabel* label = new QLabel(ui->labels);
        label->setObjectName(QStringLiteral("label_%1").arg(i));
        label->setText(QString::number(i));
        label->setProperty("class", "mark");
        //label->setFixedSize(20, label->sizeHint().height());
        label->setAlignment(Qt::AlignCenter);
        ui->labels_layout->addWidget(label);
    }

    caret = new QLineEdit(this);
    caret->setObjectName(QStringLiteral("caret"));
    caret->setGeometry(0, 4, 36, 28); // x, y, width, height
    caret->setEnabled(true);
    caret->setStyleSheet("background-color: rgb(220,220,220);");
    caret->setAlignment(Qt::AlignCenter);
    //caret->setInputMask(QString("AAA"));
    caretValidator = new QIntValidator(-maxPosition, maxPosition, caret);
    caret->setValidator(caretValidator);

    this->ui->horizontalScrollBar->setRange(-maxPosition, maxPosition);
    
    connect(this->ui->horizontalScrollBar, SIGNAL(valueChanged(int)), this, SLOT(onScrollBarValueChanged(int)));
    connect(this->caret, SIGNAL(textChanged(const QString&)), this, SLOT(caretTextChanged(const QString&)));

}

int Strip::getLength() const
{
    return 2 * this->maxPosition + 1; //this->ui->checkboxes_layout->count();
}

QVector<bool> Strip::getCheckedStates() const {
    int length = this->getLength();
    QVector<bool> result(length);
    for (int i = 0; i < length; ++i) {
        QCheckBox* checkbox = dynamic_cast<QCheckBox*>(ui->checkboxes_layout->itemAt(i)->widget());
        result[i] = checkbox->isChecked();
    }
    return result;
}

void Strip::setCheckedStates(const QVector<bool>& checkedStates) {
    int length = this->getLength();
    // TODO: rewrite when length becomes variable
    Q_ASSERT(checkedStates.size() == length);
    
    for (int i = 0; i < length; ++i) {
        QCheckBox* checkbox = qobject_cast<QCheckBox*>(ui->checkboxes_layout->itemAt(i)->widget());
        checkbox->setChecked(checkedStates[i]);
    }
}

Strip::~Strip()
{
    delete ui;
}

void Strip::resizeEvent(QResizeEvent * event)
{
    qDebug("strip resize1!!!! new size = (%d, %d)", event->size().width(), event->size().height());
    if (event->size().width() != event->oldSize().width()) // check only width change
    {
        // How much cells fit into the scrollArea visible part
        int cellsFit = event->size().width()/CHECKBOX_WIDTH;
        // Move caret to the center cell if cellsFit is odd 
        // or to the cell to the left of the center if cellsFit is even
        caret->move((cellsFit-1)/2  * CHECKBOX_WIDTH - (this->caret->width() - CHECKBOX_WIDTH)/2 + 1, caret->y());
    }
    QWidget::resizeEvent(event);
}

void Strip::onScrollBarValueChanged(int position)
{
    int cellsFit = this->width()/CHECKBOX_WIDTH;
    int cellsFitLeft = (cellsFit-1)/2; // cells that fit to the left of the caret
    int cellsNotFitLeft = (position + this->maxPosition) - cellsFitLeft;
    this->ui->scrollAreaWidgetContents->move(-cellsNotFitLeft*CHECKBOX_WIDTH, this->ui->scrollAreaWidgetContents->geometry().y());
    this->caret->setText(QString::number(this->ui->horizontalScrollBar->value()));
}

void Strip::caretTextChanged(const QString& text)
{
    bool ok;
    int number = text.toInt(&ok);

    if (ok)
    {
        this->ui->horizontalScrollBar->setValue(number);
    }
    else // when empty
    {
        qDebug("!!!!!!!! Must avoid to get into here");
        caret->setText(QString::number(this->ui->horizontalScrollBar->value()));
    }
}

void Strip::moveTo(int caretPosition)
{
    ui->horizontalScrollBar->setValue(caretPosition);
}

// Caret index is always non-negative and is the index on checkbox in vector of checkboxes
int Strip::getCaretIndex() const
{
    return this->getCaretPosition() + this->maxPosition;
}

// Caret position may be negative: it is the number on the caret label
int Strip::getCaretPosition() const {
    bool ok;
    int position = this->caret->text().toInt(&ok);
    assert(ok);
    return position;
}

void Strip::moveCaretLeft() {
    int position = this->getCaretPosition();
    this->moveTo(position - 1);
}

void Strip::moveCaretRight() {
    int position = this->getCaretPosition();
    this->moveTo(position + 1);
}

void Strip::showEvent(QShowEvent * event)
{
    // A crunch to postpone srollAreaContentsWidget initial positioning
    // because its "move" method does not position it at early stage
    // (presumably since scrollArea has not calculated it's size yet).
    // TODO: do this correctly
    this->initTimer = new QTimer();
    this->initTimer->setInterval(100);
    connect(this->initTimer, SIGNAL(timeout()), SLOT(initialPositioning()));
    this->initTimer->start();
    QWidget::showEvent(event);
}

void Strip::initialPositioning() {
    int scrollBarPosition = this->ui->horizontalScrollBar->value();
    if (scrollBarPosition == 0)
        onScrollBarValueChanged(0);
    else
        this->ui->horizontalScrollBar->setValue(0); // this will move scrollBar and trigger slideToPosition(0)
    
    this->initTimer->stop();
    this->initTimer->deleteLater();
}

void Strip::toggleCheckbox() {
    int index = this->getCaretIndex();
    QCheckBox* checkbox = dynamic_cast<QCheckBox*>(ui->checkboxes_layout->itemAt(index)->widget());
    checkbox->toggle();
}