#ifndef POSTMACHINEEXECUTOR_H
#define POSTMACHINEEXECUTOR_H

#include <QObject>
#include <QVector>
#include <QException>


struct PostMachineCommand
{
   enum Type {TYPE_NONE, TYPE_LEFT, TYPE_RIGHT, TYPE_SET_MARK, TYPE_UNSET_MARK, TYPE_CHOICE, TYPE_STOP};

   Type type;
   int ref1, // reference to the next command to execute
       ref2; // reference to the next command in a command of TYPE_CHOICE if the current cell is marked

   PostMachineCommand() : type(TYPE_NONE) {}
   PostMachineCommand(Type type, int ref1, int ref2 = -1) : type(type), ref1(ref1), ref2(ref2) {}
   QString refsToString() const;
};

QDataStream& operator<<(QDataStream&, const PostMachineCommand &);
QDataStream& operator>>(QDataStream&, PostMachineCommand &);

class PostMachineExecutorException : public QException
{
    QString message;
public:
    PostMachineExecutorException(const QString& m) : message(m) {}
    void raise() const { throw *this; }
    PostMachineExecutorException* clone() const { return new PostMachineExecutorException(*this); }
    const QString& getMessage() const { return message; }
    ~PostMachineExecutorException() throw() {}
};

class PostMachineExecutor : public QObject
{
    Q_OBJECT

    QVector<PostMachineCommand> commands;
    QVector<bool> strip;
    int caretPosition; // non-negative index of caret position
    int currentCommandIndex;
    bool running;

public:

    enum CommandExecutionStatus {COMMAND_OK = 0,
                                 ERROR_CANNOT_MOVE_LEFT,
                                 ERROR_CANNOT_MOVE_RIGHT,
                                 ERROR_CANNOT_SET_MARK,
                                 ERROR_CANNOT_UNSET_MARK,
                                 ERROR_BAD_REFERENCE};
    PostMachineExecutor();

    void clearCommands();
    void addCommand(PostMachineCommand::Type type, int ref1, int ref2 = -1);
    void setMark(int index);
    void prepareRun(const QVector<PostMachineCommand>& commands, const QVector<bool>& strip, int stripCaretPosition);
    CommandExecutionStatus executeNextCommand();
    int getCurrentCommandIndex() const { return this->currentCommandIndex; }
    bool isRunning() const { return this->running; }

Q_SIGNALS:
    void markWasSet();
    void markWasUnset();
   // void caretPositionChanged(int newIndex);
    void caretMovedLeft();
    void caretMovedRight();
    void stop();
    void goToNextCommand(int commandIndex);



};

#endif // POSTMACHINEEXECUTOR_H
