<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>CommandComboBox</name>
    <message>
        <location filename="commandcombobox.cpp" line="11"/>
        <location filename="commandcombobox.cpp" line="26"/>
        <source>left</source>
        <translation>влево</translation>
    </message>
    <message>
        <location filename="commandcombobox.cpp" line="12"/>
        <location filename="commandcombobox.cpp" line="27"/>
        <source>right</source>
        <translation>вправо</translation>
    </message>
    <message>
        <location filename="commandcombobox.cpp" line="13"/>
        <location filename="commandcombobox.cpp" line="28"/>
        <source>set mark</source>
        <translation>пометить</translation>
    </message>
    <message>
        <location filename="commandcombobox.cpp" line="14"/>
        <location filename="commandcombobox.cpp" line="29"/>
        <source>remove mark</source>
        <translation>снять метку</translation>
    </message>
    <message>
        <location filename="commandcombobox.cpp" line="15"/>
        <location filename="commandcombobox.cpp" line="30"/>
        <source>choice</source>
        <translation>выбор</translation>
    </message>
    <message>
        <location filename="commandcombobox.cpp" line="16"/>
        <location filename="commandcombobox.cpp" line="31"/>
        <source>stop</source>
        <translation>стоп</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="20"/>
        <source>Post Machine 1.0</source>
        <translation>Машина Поста 1.0</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="107"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="124"/>
        <source>&amp;File</source>
        <translation>&amp;Файл</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="134"/>
        <source>&amp;Options</source>
        <translation>&amp;Настройки</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="138"/>
        <source>&amp;Language</source>
        <translation>&amp;Язык</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="147"/>
        <source>&amp;Execution</source>
        <translation>&amp;Выполнение</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="210"/>
        <source>&amp;New</source>
        <translation>&amp;Новый</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="213"/>
        <source>Start composing a new program</source>
        <translation>Начать составлять новую программу</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="216"/>
        <source>Ctrl+N</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="221"/>
        <source>&amp;Quit</source>
        <translation>В&amp;ыход</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="229"/>
        <source>&amp;Ru</source>
        <translation>&amp;Ru</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="237"/>
        <source>&amp;En</source>
        <translation>&amp;En</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="247"/>
        <source>&amp;Run</source>
        <translation>&amp;Запустить</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="250"/>
        <source>Execute program</source>
        <translation>Выполнить программу</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="263"/>
        <source>&amp;Pause</source>
        <translation>&amp;Пауза</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="276"/>
        <source>&amp;Interrupt</source>
        <translation>П&amp;рервать</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="279"/>
        <source>Interrupt</source>
        <translation>Прервать</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="282"/>
        <source>Interrupt execution</source>
        <translation>Прервать выполнение</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="292"/>
        <source>&amp;Open...</source>
        <translation>&amp;Открыть</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="295"/>
        <source>Open file with a program</source>
        <translation>Открыть файл с программой</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="298"/>
        <source>Ctrl+O</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="308"/>
        <source>&amp;Save</source>
        <translation>Сохранить</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="311"/>
        <source>Save current program</source>
        <translation>Сохранить текущую программу</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="314"/>
        <source>Ctrl+S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="319"/>
        <source>Save as...</source>
        <translation>Сохранить как</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="322"/>
        <source>Save the program as a new file</source>
        <translation>Сохранить в другой файл</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="325"/>
        <source>Ctrl+Shift+S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="152"/>
        <source>Command</source>
        <translation>Команда</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="153"/>
        <source>Arguments</source>
        <translation>Аргументы</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="154"/>
        <source>Comment</source>
        <translation>Комментарий</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="194"/>
        <source>Commands must follow without gaps</source>
        <translation>Команды должны идти последовательно без пропусков</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="211"/>
        <source>Wrong arguments at %1!</source>
        <translation>Неверные аргументы в строке %1!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="230"/>
        <source>Error in commands sequence</source>
        <translation>Ошибка в последовательности команд</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="261"/>
        <source>Cannot move left: strip is over</source>
        <translation>Невозможно двигаться влево: лента закончилась</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="264"/>
        <source>Cannot move right: strip is over</source>
        <translation>Невозможно двигаться вправо: лента закончилась</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="267"/>
        <source>Cannot mark an already marked cell</source>
        <translation>Нельзя пометить уже помеченную ячейку</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="270"/>
        <source>Cannot unset mark at unmarked cell</source>
        <translation>Нельзя снять метку с непомеченной ячейки</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="273"/>
        <source>Wrong command reference</source>
        <translation>Неверный номер команды</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="290"/>
        <source>Execution is over!</source>
        <translation>Выполнение окончено!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="331"/>
        <source>Save program</source>
        <translation>Сохранить программу</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="333"/>
        <location filename="mainwindow.cpp" line="354"/>
        <source>Post Machine program (*.pmp);;All Files (*)</source>
        <oldsource>Post Machine program (*.pmp);*.pmp;;All Files (*)</oldsource>
        <translation>Программа для Машины Поста (*.pmp);;Все файлы (*)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="352"/>
        <source>Open program</source>
        <translation>Открыть программу</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="363"/>
        <location filename="mainwindow.cpp" line="425"/>
        <source>Unable to open file</source>
        <translation>Невозможно открыть файл</translation>
    </message>
</context>
</TS>
