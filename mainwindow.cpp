#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "strip.h"
#include "commandcombobox.h"
#include "postmachineexecutor.h"
#include <QEvent>
#include <QTranslator>
#include <QResizeEvent>
#include <QLineEdit>
#include <QMessageBox>
#include <QFileDialog>
#include <QThread>
#include <QDebug>


#include <iostream>
using std::cout;
using std::endl;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QTableWidget& table = *ui->tableWidget;
    table.setSelectionBehavior(QAbstractItemView::SelectRows);
    table.setSelectionMode(QAbstractItemView::SingleSelection);
    QPalette* palette = new QPalette();
    palette->setColor(QPalette::Highlight,Qt::blue);
    table.setPalette(*palette);


//    QHeaderView *verticalHeader = table->verticalHeader();
    table.setHorizontalHeaderItem(0, new QTableWidgetItem());
    table.setHorizontalHeaderItem(1, new QTableWidgetItem());
    table.setHorizontalHeaderItem(2, new QTableWidgetItem());
    table.setColumnWidth(0, 130);
    table.setColumnWidth(1, 100);

    this->comboBoxesSignalMapper = new QSignalMapper(ui->tableWidget);
    connect(this->comboBoxesSignalMapper, SIGNAL(mapped(QWidget*)), this, SLOT(onActionComboIndexChanged(QWidget*)));

    for (int row = 0; row < MainWindow::INITIAL_ROWS_CNT;  ++row) {
        this->addRow();
    }

  //  this->setGeometry(QRect(QPoint(40,40), QSize(800, 600)));

    QActionGroup* languagesActionGroup = new QActionGroup(this);
    languagesActionGroup->addAction(this->ui->actionEn);
    languagesActionGroup->addAction(this->ui->actionRu);
    this->ui->actionEn->setData(QString("en"));
    this->ui->actionRu->setData(QString("ru_RU"));

    connect(languagesActionGroup, SIGNAL(triggered(QAction*)), this, SLOT(languageIsChanged(QAction*)));

    this->ui->actionRu->trigger(); // action is setChecked(true) if isCheckable

    connect(this->ui->tableWidget, SIGNAL(cellChanged(int,int)), this, SLOT(onCellChanged(int,int)));
    this->ui->tableWidget->installEventFilter(this);

    connect(this->ui->action_Run, SIGNAL(triggered(bool)), this, SLOT(executeProgram()));
    
    connect(this->ui->action_new, SIGNAL(triggered(bool)), this, SLOT(onNewAction()));
    connect(this->ui->action_open, SIGNAL(triggered(bool)), this, SLOT(onOpenAction()));
    connect(this->ui->action_save, SIGNAL(triggered(bool)), this, SLOT(onSaveAction()));
    connect(this->ui->action_saveas, SIGNAL(triggered(bool)), this, SLOT(onSaveAsAction()));

    this->timer = new QTimer(this);
    this->timer->setInterval(1000);
    connect(this->timer, SIGNAL(timeout()), this, SLOT(timerTimeout()));

    this->executor = new PostMachineExecutor();
    
    connect(this->executor, SIGNAL(caretMovedLeft()), this->ui->strip, SLOT(moveCaretLeft()));
    connect(this->executor, SIGNAL(caretMovedRight()), this->ui->strip, SLOT(moveCaretRight()));
    connect(this->executor, SIGNAL(markWasSet()), this->ui->strip, SLOT(toggleCheckbox()));
    connect(this->executor, SIGNAL(markWasUnset()), this->ui->strip, SLOT(toggleCheckbox()));
}

void MainWindow::onCellChanged(int row, int column)
{
    if (column == 1 && row == this->ui->tableWidget->rowCount() - 1)
        addRow();
    //cout << "onCellChagned, row-column = " << row << "-" << column << endl;
}


void MainWindow::onActionComboIndexChanged(QWidget* cellWidget)
{
    int row = this->ui->tableWidget->rowAt(cellWidget->y());
    if (row == this->ui->tableWidget->rowCount() - 1)
       this->addRow();
}

void MainWindow::addRow()
{
    int newRowIndex = this->ui->tableWidget->rowCount();
    this->ui->tableWidget->insertRow(newRowIndex);

    CommandComboBox* combo = new CommandComboBox(this->ui->tableWidget);
    this->ui->tableWidget->setCellWidget(newRowIndex, 0, combo);
    connect(combo, SIGNAL(currentIndexChanged(int)), comboBoxesSignalMapper, SLOT(map()));
    comboBoxesSignalMapper->setMapping(combo, combo);

    disconnect(this->ui->tableWidget, SIGNAL(cellChanged(int,int)), this, SLOT(onCellChanged(int,int)));
    this->ui->tableWidget->setItem(newRowIndex, 1, new QTableWidgetItem(""));
    connect(this->ui->tableWidget, SIGNAL(cellChanged(int,int)), this, SLOT(onCellChanged(int,int)));
}


void MainWindow::languageIsChanged(QAction *languageAction)
{
    const QString& lang = languageAction->data().toString();
    qApp->removeTranslator(&m_translator);

    if (m_translator.load(QString("postmachine_%1").arg(lang)))
        qApp->installTranslator(&m_translator);
}

MainWindow::~MainWindow()
{
    delete ui;
    delete executor;
    delete timer;
}

void MainWindow::changeEvent(QEvent * event)
{
    if (event->type() == QEvent::LanguageChange)
    {
        this->retranslateUi();
    }
    else
    {
        QWidget::changeEvent(event);
    }
}


void MainWindow::resizeEvent(QResizeEvent * event)
{
    qDebug("Window resize, width = %d", event->size().width());
    QWidget::resizeEvent(event);
}

void MainWindow::retranslateUi()
{
    this->ui->retranslateUi(this);

    ui->tableWidget->horizontalHeaderItem(0)->setText(this->tr("Command"));
    ui->tableWidget->horizontalHeaderItem(1)->setText(this->tr("Arguments"));
    ui->tableWidget->horizontalHeaderItem(2)->setText(this->tr("Comment"));

    for (int i = 0; i < this->ui->tableWidget->rowCount(); ++i)
    {
       qobject_cast<CommandComboBox*>(this->ui->tableWidget->cellWidget(i, 0))->retranslateUi();
    }
}

bool MainWindow::eventFilter(QObject* object, QEvent* event)
{
    if (event->type() == QEvent::KeyPress && object == this->ui->tableWidget)
    {
        QKeyEvent* pKeyEvent = static_cast<QKeyEvent*>(event);
        if (pKeyEvent->key() == Qt::Key_Delete)
        {
            if (this->ui->tableWidget->rowCount() > 1)
            {
                int currentRow = this->ui->tableWidget->currentRow();
                qDebug() << "rowNumber = " << currentRow << endl;
                this->ui->tableWidget->removeRow(currentRow);
            }
            return true;
        }
        qDebug() << "Event filter: other key pressed";
    }
    return QWidget::eventFilter(object, event);
}

QVector<PostMachineCommand> MainWindow::getCommands() const
{
    QVector<PostMachineCommand> commands;
    bool wasEmpty = false;
    for (int row = 0, rowCount = this->ui->tableWidget->rowCount(); row < rowCount; ++row)
    {
        CommandComboBox* ccb = qobject_cast<CommandComboBox*>(this->ui->tableWidget->cellWidget(row, 0));
        if (ccb->currentIndex() == 0) {
            wasEmpty = true;
            continue;
        }
        if (ccb->currentIndex() != 0 && wasEmpty) {
           throw CommandsFormatException(this->tr("Commands must follow without gaps"));
        }

        QTableWidgetItem* item = this->ui->tableWidget->item(row, 1);
        QString text = item ? item->text() : "";
        QStringList refs = text.split(QString(","), QString::SkipEmptyParts);

        for (int i = 0; i < refs.size(); ++i)
            refs[i] = refs[i].trimmed();

        int intRefs[2] = {-1, -1};

        for (int i = 0; i < 2 && i < refs.size(); ++i) {
            bool ok;
            intRefs[i] = refs[i].toInt(&ok) - 1;

            if (!ok) {
                throw CommandsFormatException(this->tr("Wrong arguments at %1!").arg(row+1));
            }
        }

        PostMachineCommand::Type commandType = (PostMachineCommand::Type)ccb->currentIndex();
        commands.push_back(PostMachineCommand(commandType, intRefs[0], intRefs[1]));
    }
    
    return commands;
}

void MainWindow::executeProgram()
{
    QVector<PostMachineCommand> commands;
    try {
        commands = this->getCommands();
    }
    catch(CommandsFormatException& ex) {
        QMessageBox mb;
        mb.setWindowTitle(this->tr("Error in commands sequence"));
        mb.setText(ex.getMessage());
        mb.exec();
        return;
    }
    
    executor->prepareRun(commands, this->ui->strip->getCheckedStates(), this->ui->strip->getCaretIndex());
    qDebug("after prepareRun(): caretPosition = %d", this->ui->strip->getCaretIndex());

    this->ui->tableWidget->setEnabled(false);
    timer->start();

    //TODO: this is wrong place for setEnabled(true)
    this->ui->tableWidget->setEnabled(true);
}

void MainWindow::timerTimeout()
{
    int commandIndex = executor->getCurrentCommandIndex();
    cout << "commandIndex = " << commandIndex << endl;
    this->ui->tableWidget->selectRow(commandIndex);

    PostMachineExecutor::CommandExecutionStatus status = executor->executeNextCommand();
    if (status != PostMachineExecutor::COMMAND_OK)
    {
        this->timer->stop();

        QString message;
        switch (status)
        {
            case PostMachineExecutor::ERROR_CANNOT_MOVE_LEFT:
                message = this->tr("Cannot move left: strip is over");
                break;
            case PostMachineExecutor::ERROR_CANNOT_MOVE_RIGHT:
                message = this->tr("Cannot move right: strip is over");
                break;
            case PostMachineExecutor::ERROR_CANNOT_SET_MARK:
                message = this->tr("Cannot mark an already marked cell");
                break;
            case PostMachineExecutor::ERROR_CANNOT_UNSET_MARK:
                message = this->tr("Cannot unset mark at unmarked cell");
                break;
            case PostMachineExecutor::ERROR_BAD_REFERENCE:
                message = this->tr("Wrong command reference");
                break;
            case PostMachineExecutor::COMMAND_OK: // to suppress compiler warning
                break;
        }

        QMessageBox mb;
        mb.setText(message);
        mb.exec();
        return;
    }

    if (!executor->isRunning())
    {
        this->timer->stop();

        QMessageBox mb;
        mb.setText(this->tr("Execution is over!"));
        mb.exec();
    }

}


void MainWindow::onNewAction()
{
    while (this->ui->tableWidget->rowCount() < INITIAL_ROWS_CNT) {
        this->addRow();
    }
    
    while (this->ui->tableWidget->rowCount() > INITIAL_ROWS_CNT){
        this->ui->tableWidget->removeRow(0);
    }
      
    for (int row = 0; row < INITIAL_ROWS_CNT;  ++row)
    {
        CommandComboBox* cbox = qobject_cast<CommandComboBox*>(this->ui->tableWidget->cellWidget(row, 0));
        cbox->setCurrentIndex(0);
        QTableWidgetItem* item = this->ui->tableWidget->item(row, 1);
        item->setText("");
    }
    //TODO: clear strip; set caretPosition to 0
    this->openedFilePath.clear();
}

void MainWindow::onSaveAction()
{
    if (this->openedFilePath.isNull())
        this->onSaveAsAction();
    else
        this->saveByPath(this->openedFilePath);
}

void MainWindow::onSaveAsAction()
{
    QString selectedFilter;
    QString saveFileName = QFileDialog::getSaveFileName(
            this,
            this->tr("Save program"),
            QString(),
            this->tr("Post Machine program (*.pmp);;All Files (*)"),
            &selectedFilter
    );
    
    if (selectedFilter == "Post Machine program (*.pmp)"
        && !saveFileName.endsWith(".pmp")) {
            saveFileName += ".pmp";
    }
    qDebug() << saveFileName;
    this->saveByPath(saveFileName);
    this->openedFilePath = saveFileName;
            
    return;
}

void MainWindow::onOpenAction()
{
    QString openFileName = QFileDialog::getOpenFileName(
            this,
            this->tr("Open program"),
            QString(),
            this->tr("Post Machine program (*.pmp);;All Files (*)")
    );
    if (openFileName.isEmpty())
        return;

    QFile file(openFileName);
    if (!file.open(QIODevice::ReadOnly)) {
        QMessageBox::information(
            this,
            tr("Unable to open file"),
            file.errorString()
        );
        return;
    }
    
    this->openedFilePath = openFileName;
        
    QVector<PostMachineCommand> commands;
    QVector<bool> checkedStates;
    qint32 caretPosition;
    
    QDataStream in(&file);
    in.setVersion(QDataStream::Qt_5_5);
    in >> commands >> checkedStates >> caretPosition;
    this->restoreProgramInterface(commands, checkedStates, caretPosition);
}

void MainWindow::restoreProgramInterface(const QVector<PostMachineCommand>& commands,
                                         const QVector<bool>& checkedStates,
                                         int caretPosition)
{
    
    for (int row = 0; row < this->ui->tableWidget->rowCount();  ++row)
    {
        QTableWidgetItem* w = this->ui->tableWidget->item(row, 1);
        qDebug() << w->text();
    }

    int needRowsCnt = commands.size() + 1;
    
    while (this->ui->tableWidget->rowCount() < needRowsCnt) {
        this->addRow();
        qDebug() << "TableWidget size = " << this->ui->tableWidget->rowCount();
    }
    
    while (this->ui->tableWidget->rowCount() > needRowsCnt){
        this->ui->tableWidget->removeRow(0);
        qDebug() << "TableWidget size = " << this->ui->tableWidget->rowCount();
    }
      
    for (int row = 0; row < commands.size();  ++row)
    {
        CommandComboBox* cbox = qobject_cast<CommandComboBox*>(this->ui->tableWidget->cellWidget(row, 0));
        cbox->setCurrentIndex(commands[row].type);
        QTableWidgetItem* item = this->ui->tableWidget->item(row, 1);
        item->setText(commands[row].refsToString());
    }
    
    this->ui->strip->setCheckedStates(checkedStates);
    this->ui->strip->moveTo(caretPosition);
}

void MainWindow::saveByPath(const QString& path)
{
    if (path.isEmpty())
        return;
   
    QFile file(path);
    if (!file.open(QIODevice::WriteOnly)) {
        QMessageBox::information(
            this,
            tr("Unable to open file"),
            file.errorString()
        );
        return;
    }

    QDataStream out(&file);
    out.setVersion(QDataStream::Qt_5_5);

    QVector<PostMachineCommand> commands = this->getCommands();
    out << commands;
    out << this->ui->strip->getCheckedStates();
    out << (qint32)this->ui->strip->getCaretPosition();
    file.close();
}