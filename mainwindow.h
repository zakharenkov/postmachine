#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTranslator>
#include <QTableWidgetItem>
#include <QSignalMapper>
#include <QTimer>
#include "postmachineexecutor.h"

namespace Ui {
class MainWindow;
}

class CommandsFormatException : public QException
{
    QString message;
public:
    CommandsFormatException(const QString& m) : message(m) {}
    void raise() const { throw *this; }
    CommandsFormatException* clone() const { return new CommandsFormatException(*this); }
    const QString& getMessage() const { return message; }
    ~CommandsFormatException() throw() {}
};

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

protected Q_SLOTS:
    void languageIsChanged(QAction*);

    void onCellChanged(int row, int column);
    //void onItemChanged(QTableWidgetItem* item);
    void onActionComboIndexChanged(QWidget* cellWidget);

    void executeProgram();
    void timerTimeout();
    
    void onNewAction();
    void onOpenAction();
    void onSaveAction();
    void onSaveAsAction();

protected:
    virtual void changeEvent(QEvent * event);
    virtual void resizeEvent(QResizeEvent * event);
    bool eventFilter(QObject* object, QEvent* event);

    enum Languages {LANGUAGE_RU, LANGUAGE_EN};
    const static int INITIAL_ROWS_CNT = 10;

    void retranslateUi();
    void addRow();
    
    /**
     * @throw If rude format errors exist then throws a CommandsFormatException.
     * @return a vector of commands retrieved from the interface.
     */
    QVector<PostMachineCommand> getCommands() const;
    
    void saveByPath(const QString& path);
    
    void restoreProgramInterface(const QVector<PostMachineCommand>& commands,
                                 const QVector<bool>& checkedStates,
                                 int caretPosition);
   

    QSignalMapper* comboBoxesSignalMapper;

private:
    QTranslator m_translator;
    PostMachineExecutor* executor;
    QTimer* timer;
    QString openedFilePath;

public:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
