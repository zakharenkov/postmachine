#ifndef COMMANDCOMBOBOX_H
#define COMMANDCOMBOBOX_H

#include <QComboBox>

class CommandComboBox : public QComboBox
{
    Q_OBJECT

public:
    CommandComboBox(QWidget* parent = 0);
    ~CommandComboBox();

    void retranslateUi();
};

#endif // COMMANDCOMBOBOX_H
